import pylab
import numpy as np
from numpy import *
import math
from pylab import *
import subprocess
from subprocess import call
# Getting the data from a file
m = genfromtxt("covs_05.dat")
# Name of the output file
title = "star-site-cov-05.eps"
#Boundingbox - defines the size of the figure in points
BBLL = array([0, 0]) #lower left corner
BBUR = array([440, 140]) #upper right corner
# Opening the file and writing the eps header
file = open(title, "w")
file.write("%!PS-Adobe-3.0 EPSF-1.2\n")
file.write("%%BoundingBox: {} {} {} {}\n\n" .format(BBLL[0], BBLL[1], BBUR[0], BBUR[1]))
file.write("/Times-Roman findfont 12 scalefont setfont\n\n")
file.write("1 setlinejoin\n\n")
# Positioning the origin on the page
file.write("20 20 translate\n\n")
# Drawing the line representing the sites
file.write("0 -1 moveto\n")
file.write("400 -1 lineto\n")
file.write("stroke\n\n")
# Drawing the tic marks for sites and nucleotides within each site
for i in range(40):
    locx = 10*i
    file.write("{} -1 moveto\n" .format(locx))
    file.write("0 -5 rlineto\n")
    file.write("stroke\n")
    for j in range(4):
        locx = 10*i + 2*(j+1)
        file.write("{} -1 moveto\n" .format(locx))
        file.write("0 -2 rlineto\n")
        file.write("stroke\n")
# Drawing the curved lines connecting nucleotides at sites.
# There were 32 pairs in this case
for i in range(32):
    llocx = 10.*m[i][0] + 2*(m[i][2]+1)
    llocy = 0
    rlocx = 10.*m[i][1] + 2*(m[i][3]+1)
    rlocy = 0
    d = rlocx - llocx
    file.write("gsave\n")
    file.write("{} 0 translate\n\n" .format(llocx))
    if m[i][4] > 0:
        file.write("1 1 0 0 setcmykcolor\n")
    if m[i][4] < 0:
        file.write("0 0 1 .3 setcmykcolor\n")
#The following lines define Bezier curves connecting the sites/nucleotides
    file.write("0 0 moveto\n")
    file.write("{} {}\n" .format(d/3, d/3))
    file.write("{} {}\n" .format(2*d/3, d/3))
    file.write("{} 0 curveto\n" .format(d))
    file.write("stroke\n\n")
    file.write("grestore\n\n")

file.write("showpage\n")
file.close()
# This line converts the eps file to a pdf file.
# It might work only on Linux
call(["epstopdf", title])