import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid.axislines import SubplotZero

df = pd.read_csv("target_off_regressions_weights.csv")

#x = df['Weight']
#x.fillna(value=0, inplace=True)

df = df[['Nucleotide', 'Weight','C','U','A','G']]
print(df)
x = df['Weight']
x.fillna(value=0, inplace=True)
x_vals = x[:4]

#print(x_vals)

x_array = x_vals.to_numpy()
print(x_array[0]) #check to see if this gives the first value

#mean of the x values
x1 = 0
for i in range(len(x_array)):
    x1 += x_array[i]/(len(x_array))
print(x1)

#converting columns to arrays
y_c = df['C']
y_u = df['U']
y_a = df['A']
y_g = df['G']

yc_array = y_c.to_numpy()
yu_array = y_u.to_numpy()
ya_array = y_a.to_numpy()
yg_array = y_g.to_numpy()


y_all = np.concatenate((yc_array, yu_array, ya_array, yg_array), axis=None)
#print(y_all)

#mean of the y values
y_all_m = 0
for i in range(len(y_all)):
    y_all_m += y_all[i]/(len(y_all))

#print(y_all_m)

#didn't need this
# y2_array = []*(len(y_all))
# x2_array = []*(len(x_array))
# for j in range(len(x_array)):
#     x2 = x_array[j] - x1
#     x2_array.append(x2)
# for i in range(len(y_all)):
#     y2 = y_all[i] - y_all_m
#     y2_array.append(y2)


'''
computing products between weight of C and regressions on C
'''
prod_array_c = []
prod_c = 0.0
for i in range(len(yc_array)):
    prod_c = x_array[0] * yc_array[i]
    prod_array_c.append(prod_c)

#print(len(prod_array_c))
#print(prod_array_c)

'''
computing products between weight of U and regressions on U
'''
prod_array_u = []
prod_u = 0.0
for i in range(len(yu_array)):
    prod_u = x_array[1] * yu_array[i]
    prod_array_u.append(prod_u)

#print(len(prod_array_u))
#print(prod_array_u)

'''
computing products between weight of A and regressions on A
'''
prod_array_a = []
prod_a = 0.0
for i in range(len(ya_array)):
    prod_a = x_array[2] * ya_array[i]
    prod_array_a.append(prod_a)

#print(len(prod_array_a))
#print(prod_array_a)

'''
computing products between weight of G and regressions on G
'''
prod_array_g = []
prod_g = 0.0
for i in range(len(yg_array)):
    prod_g = x_array[3] * yg_array[i]
    prod_array_g.append(prod_g)

#print(len(prod_array_g))
#print(prod_array_g)

#convert all the products into one big array
all_products = np.concatenate((prod_array_c, prod_array_u, prod_array_a, prod_array_g), axis=None)
#print(all_products)

#mean_prod = [] #final list of mean products divided by the number there are
mean_products = 0.0
for i in range(len(all_products)):
    mean_products += all_products[i]/(len(all_products))
#print(mean_products)


#mean of the products - product of the  means = cov
# for i in range(len(mean_prod)):
cov = mean_products - (x1*y_all_m)
print("cov = %f" % cov)

#print("cov = " + str(cov))

''' 
calculating variance in x
'''
mean_of_squares_x = 0.0
for i in range(len(x_array)):
    mean_of_squares_x += x_array[i]**2/(len(x_array))
print(mean_of_squares_x)

#variance
var = mean_of_squares_x - (x1**2)
print("var = %f" % var)
#print("var =") + str(var)

'''
calculating regression
'''
reg = cov/var
print("reg = %f" % reg)
#print(str("reg =") + reg)

'''
computing y intercept of the regression line
'''
#intercept = y(bar) - reg*x(bar)
y_int = y_all_m - (reg*x1)
print("y_int = %f" % y_int)

'''
equation of the regression line
'''
def y(x):
    return y_int + (reg*x)

#plt.show()


#making the plot
fig, ax = plt.subplots(num = 100, figsize = (9, 5), clear = True)
N = 40;     col = ['b', 'y', 'r', 'g', 'c'];    Molecules = ['C', 'U', 'A', 'G']

# Plot scatters & line...
for i in range(len(x_vals)):
    ax.scatter(np.tile(df.Weight[i], (N, 1)), df.iloc[:, i+2], s = 16, c = col[i], marker = 'o', label = Molecules[i])

#plotting the line in cyan color, col[4]
xx_val = np.r_[109:154]
ax.plot(xx_val, y(xx_val), col[4] + '-.', linewidth = 2, label = 'Regression Line')

# Put legend...
handles, labels = ax.get_legend_handles_labels()
handles, labels = np.roll(handles, shift = -1), np.roll(labels, shift = -1)     # Puts 'Regression Line' last
prop_dict = {'size': 8.5, 'weight': 'semibold', 'family': 'serif', 'style': 'normal', 'stretch': 'condensed'}
ax.legend(handles, labels, loc = 'upper left', ncol = 3, prop = prop_dict)

# Set axes...
#ax.set_ylim(-0.5, 1)
ax.set_xlim(xx_val[0]-1, xx_val[-1]+1);
ax.set_xticks(x_vals)               # Ticks to be kept
ax.set_xticklabels(x_vals.astype(int), fontsize = 9, rotation = -30, rotation_mode = 'anchor', ha = 'left')
ax.set_ylim(-5000,10000)
ax.set_yticklabels(ax.get_yticks()/1e4, fontsize = 9)   # Scales y-axis for convenience

#plt.ylim(-0.5,1)

ax.set_xlabel('Molecular Weights', fontsize = 10, weight = 'semibold', family = 'serif')
ax.set_ylabel('Regressions of Off Values($\\times 10^4$)', fontsize = 10, weight = 'semibold', family = 'monospace')
ax.set_title('Molecular weights vs. Regressions on Sites', fontsize = 10, weight = 'semibold', family = 'serif')

fig.show()
#plt.savefig('target-rna-Weights-vs-rFon1D-3.png')

# line = plt.plot(x, y(x),'co') #this works to just show the line
# plt.show()
