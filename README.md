This repo contains the data and scripts for the paper titled *Analyzing Genomic Data Using Tensor-Based Orthogonal Polynomials with Application to Synthetic RNAs*. Please read the full paper [here](https://academic.oup.com/nargab/article/2/4/lqaa101/6030984). This paper uses the regulatory RNA sequence data from *Computational design of smalltranscription activating RNAs for versatile and dynamic gene regulation* by [Chappell et al](https://www.nature.com/articles/s41467-017-01082-6). 

The code folder contains the python scripts and data used to compute the tensor-based orthogonal polynomials and plot the figures as seen in the paper.
 
